from django.shortcuts import render
from rest_framework import viewsets
from .serializers import ExampleTodoSerializer
from .models import Exampletodo
# Create your views here.
class ExampletodoView(viewsets.ModelViewSet):
      serializer_class = ExampleTodoSerializer
      queryset = Exampletodo.objects.all()
