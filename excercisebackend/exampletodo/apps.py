from django.apps import AppConfig


class ExampletodoConfig(AppConfig):
    name = 'exampletodo'
