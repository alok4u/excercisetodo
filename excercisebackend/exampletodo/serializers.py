from rest_framework import serializers
from .models import Exampletodo
class ExampleTodoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Exampletodo
    fields = ('id','description', 'duedate', 'state')
