# Generated by Django 2.2.6 on 2019-11-19 08:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exampletodo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exampletodo',
            name='state',
            field=models.CharField(choices=[(0, 'todo'), (1, 'in-progress'), (2, 'done')], max_length=1),
        ),
    ]
