from django.db import models

# Create your models here.
class Exampletodo(models.Model):
      TODO_STATUS = 1
      PROGRESS_STATUS = 2
      DONE_STATUS = 3
      STATUS_CHOICES = (
        (TODO_STATUS, 'todo'),
        (PROGRESS_STATUS, 'in-progress'),
        (DONE_STATUS, 'done'),
      )
      description = models.TextField()
      duedate = models.DateField()
      state = models.IntegerField(choices=STATUS_CHOICES, default=TODO_STATUS)
      def _str_(self):
        return self.description
