from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Exampletodo 

class ExampletodoAdmin(admin.ModelAdmin):
 list_display = ('description', 'duedate' , 'state')

    # Register your models here.
admin.site.register(Exampletodo, ExampletodoAdmin)
